﻿using SiteSerralheriaSantos.Models;
using SiteSerralheriaSantos.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace SiteSerralheriaSantos.Repository
{
    public class MensagemRepository : RepositorioBase<Mensagem>
    {
        public Mensagem BuscarPorId(Guid mensagemId)
        {
            return DbSet.Include(x => x.Empresa).FirstOrDefault(c => c.MensagemId == mensagemId);
        }

        public IEnumerable<Mensagem> BuscarTodos()
        {
            var usu = Context.Usuario.FirstOrDefault(x => x.UsuarioId == Account.UsuarioId);
            var mensagem = (Account.Roles == "Master") ? DbSet : DbSet.Where(x => x.EmpresaId == usu.EmpresaId);
            return mensagem;
        }

        public int QntDeMsgNaoVisualizada()
        {
            var usu = Context.Usuario.FirstOrDefault(x => x.UsuarioId == Account.UsuarioId);
            var qntMsg = (Account.Roles == "Master") ? DbSet.ToList().Count(x => x.Visto == false) : DbSet.Where(x => x.EmpresaId == usu.EmpresaId).ToList().Count(x => x.Visto == false);
            return qntMsg;
        }

        public IEnumerable<Mensagem> BuscarTodosPorData(DateTime data)
        {
            var usu = Context.Usuario.FirstOrDefault(x => x.UsuarioId == Account.UsuarioId);
            var mensagem = (Account.Roles == "Master") ? DbSet.Where(x => x.DataDeEntrada == data) : DbSet.Where(x => x.EmpresaId == usu.EmpresaId && x.DataDeEntrada == data);
            return mensagem;
        }

        public IEnumerable<Mensagem> BuscarTodosPorMensagem(string textoDigitado)
        {
            var usu = Context.Usuario.FirstOrDefault(x => x.UsuarioId == Account.UsuarioId);
            var mensagem = (Account.Roles == "Master") ? DbSet.Where(x => x.Nome.StartsWith(textoDigitado)) : DbSet.Where(x => x.EmpresaId == usu.EmpresaId && x.Nome.StartsWith(textoDigitado));
            return mensagem;
        }
    }
}