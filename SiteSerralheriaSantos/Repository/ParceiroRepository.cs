﻿using SiteSerralheriaSantos.Models;
using SiteSerralheriaSantos.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace SiteSerralheriaSantos.Repository
{
    public class ParceiroRepository : RepositorioBase<Parceiro>
    {
        public Parceiro BuscarPorId(Guid parceiroId)
        {
            return DbSet.Include(x => x.Imagem).FirstOrDefault(c => c.ParceiroId == parceiroId);
        }

        public IEnumerable<Parceiro> BuscarTodos()
        {
            var usu = Context.Usuario.FirstOrDefault(x => x.UsuarioId == Account.UsuarioId);
            var parceiros = (Account.Roles == "Master") ? DbSet : DbSet.Where(x => x.EmpresaId == usu.EmpresaId);
            return parceiros;
        }

        public IEnumerable<Parceiro> BuscarTodosPorEmpresa(Guid empresaId)
        {
            return DbSet.Include(x => x.Imagem).Where(x => x.EmpresaId == empresaId).ToList();
        }

        public IEnumerable<Parceiro> BuscarTodosPorParceiro(string textoDigitado)
        {
            var usu = Context.Usuario.FirstOrDefault(x => x.UsuarioId == Account.UsuarioId);
            var parceiros = (Account.Roles == "Master") ? DbSet.Where(x => x.Nome.StartsWith(textoDigitado)) : DbSet.Where(x => x.EmpresaId == usu.EmpresaId && x.Nome.StartsWith(textoDigitado));
            return parceiros;
        }
    }
}