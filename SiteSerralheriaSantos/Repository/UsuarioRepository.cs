﻿using SiteSerralheriaSantos.Models;
using SiteSerralheriaSantos.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace SiteSerralheriaSantos.Repository
{
    public class UsuarioRepository : RepositorioBase<Usuario>
    {
        public Usuario BuscarPorId(Guid id)
        {
            return Context.Usuario.FirstOrDefault(c => c.UsuarioId == id);
        }

        public Usuario BuscarPorLogin(string login)
        {
            return DbSet.Include(x => x.Empresa).FirstOrDefault(c => c.Login == login);
        }

        public IEnumerable<Usuario> BuscarTodos()
        {
            var usu = Context.Usuario.FirstOrDefault(x => x.UsuarioId == Account.UsuarioId);
            var usuarios = (Account.Roles == "Master") ? DbSet : DbSet.Where(x => x.EmpresaId == usu.EmpresaId);
            return usuarios;
        }

        public IEnumerable<Usuario> BuscarTodosPorNome(string textoDigitado)
        {
            var usu = Context.Usuario.FirstOrDefault(x => x.UsuarioId == Account.UsuarioId);
            var usuarios = (Account.Roles == "Master") ? DbSet.Where(x => x.Nome.StartsWith(textoDigitado)) : DbSet.Where(x => x.EmpresaId == usu.EmpresaId && x.Nome.StartsWith(textoDigitado));
            return usuarios;
        }
    }
}