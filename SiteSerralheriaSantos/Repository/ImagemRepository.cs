﻿using SiteSerralheriaSantos.Models;
using System;
using System.Linq;

namespace SiteSerralheriaSantos.Repository
{
    public class ImagemRepository : RepositorioBase<Imagem>
    {
        public Imagem BuscarPorId(Guid imagemId)
        {
            return DbSet.FirstOrDefault(x => x.ImagemId == imagemId);
        }
    }
}