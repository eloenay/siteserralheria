﻿using SiteSerralheriaSantos.Models;
using SiteSerralheriaSantos.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace SiteSerralheriaSantos.Repository
{
    public class EmpresaRepository : RepositorioBase<Empresa>
    {
        public IEnumerable<Empresa> BuscarTodos()
        {
            var empresas = (Account.Roles == "Master") ? DbSet : DbSet.Where(x => x.Usuarios.FirstOrDefault().UsuarioId == Account.UsuarioId);
            return empresas;
        }

        public Empresa BuscarPorId(Guid empresaId)
        {
            return DbSet.Include(x => x.RedesSociais).Include(x => x.Enderecos).Include(x => x.Imagem).FirstOrDefault(x => x.EmpresaId == empresaId);
        }

        public IEnumerable<Empresa> BuscarTodosPorEmpresa(string textoDigitado)
        {
            var empresas = (Account.Roles == "Master") ? DbSet.Where(x => x.Nome.StartsWith(textoDigitado)) : DbSet.Where(x => x.Usuarios.FirstOrDefault().UsuarioId == Account.UsuarioId && x.Nome.StartsWith(textoDigitado));
            return empresas;
        }

        public Empresa BuscarEmpresaParaTelaInicial()
        {
            return DbSet.Include(x => x.RedesSociais).Include(x => x.Enderecos).Include(x => x.Imagem).Include(x => x.Parceiros.Select(p => p.Imagem)).Include(x => x.Galerias.Select(g => g.Imagem)).FirstOrDefault();
        }
    }
}