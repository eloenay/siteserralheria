﻿using SiteSerralheriaSantos.Models;
using SiteSerralheriaSantos.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace SiteSerralheriaSantos.Repository
{
    public class EquipeRepository : RepositorioBase<Equipe>
    {
        public Equipe BuscarPorId(Guid equipeId)
        {
            return DbSet.Include(x => x.Imagem).FirstOrDefault(c => c.EquipeId == equipeId);
        }

        public IEnumerable<Equipe> BuscarTodos()
        {
            var usu = Context.Usuario.FirstOrDefault(x => x.UsuarioId == Account.UsuarioId);
            var galerias = (Account.Roles == "Master") ? DbSet : DbSet.Where(x => x.EmpresaId == usu.EmpresaId);
            return galerias;
        }

        public IEnumerable<Equipe> BuscarTodosPorEquipe(string textoDigitado)
        {
            var usu = Context.Usuario.FirstOrDefault(x => x.UsuarioId == Account.UsuarioId);
            var galerias = (Account.Roles == "Master") ? DbSet.Where(x => x.Nome.StartsWith(textoDigitado)) : DbSet.Where(x => x.EmpresaId == usu.EmpresaId && x.Nome.StartsWith(textoDigitado));
            return galerias;
        }
    }
}