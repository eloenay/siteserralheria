﻿using SiteSerralheriaSantos.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SiteSerralheriaSantos.Repository
{
    public class RedeSocialRepository : RepositorioBase<RedeSocial>
    {
        public RedeSocial BuscarPorId(Guid redesocialId)
        {
            return DbSet.FirstOrDefault(x => x.RedeSocialId == redesocialId);
        }

        public IEnumerable<RedeSocial> BuscarTodosRedeSocialPorEmpresa(Guid empresaId)
        {
            return DbSet.Where(x => x.EmpresaId == empresaId).ToList();
        }
    }
}