﻿using SiteSerralheriaSantos.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SiteSerralheriaSantos.Repository
{
    public class EnderecoRepository : RepositorioBase<Endereco>
    {
        public Endereco BuscarPorId(Guid enderecoId)
        {
            return DbSet.FirstOrDefault(x => x.EnderecoId == enderecoId);
        }

        public IEnumerable<Endereco> BuscarTodosEnderecoPorEmpresa(Guid empresaId)
        {
            return DbSet.Where(x => x.EmpresaId == empresaId).ToList();
        }
    }
}