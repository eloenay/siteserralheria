﻿using SiteSerralheriaSantos.Models;
using System.Collections.Generic;
using System.Linq;

namespace SiteSerralheriaSantos.Repository
{
    public class CategoriaRepository : RepositorioBase<Categoria>
    {
        public IEnumerable<Categoria> BuscarTodos()
        {
            return DbSet;
        }
    }
}