﻿using SiteSerralheriaSantos.Models;
using SiteSerralheriaSantos.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace SiteSerralheriaSantos.Repository
{
    public class GaleriaRepository : RepositorioBase<Galeria>
    {
        public Galeria BuscarPorId(Guid galeriaId)
        {
            return DbSet.Include(x => x.Imagem).FirstOrDefault(c => c.GaleriaId == galeriaId);
        }

        public IEnumerable<Galeria> BuscarTodos()
        {
            var usu = Context.Usuario.FirstOrDefault(x => x.UsuarioId == Account.UsuarioId);
            var galerias = (Account.Roles == "Master") ? DbSet : DbSet.Where(x => x.EmpresaId == usu.EmpresaId);
            return galerias;
        }

        public IEnumerable<Galeria> BuscarTodosPorEmpresa(Guid empresaId)
        {
            return DbSet.Include(x => x.Imagem).Where(x => x.EmpresaId == empresaId).ToList();
        }

        public IEnumerable<Galeria> BuscarTodosPorGaleria(string textoDigitado)
        {
            var usu = Context.Usuario.FirstOrDefault(x => x.UsuarioId == Account.UsuarioId);
            var galerias = (Account.Roles == "Master") ? DbSet.Where(x => x.Titulo.StartsWith(textoDigitado)) : DbSet.Where(x => x.EmpresaId == usu.EmpresaId && x.Titulo.StartsWith(textoDigitado));
            return galerias;
        }
    }
}