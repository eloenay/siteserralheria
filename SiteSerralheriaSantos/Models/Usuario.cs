﻿using SiteSerralheriaSantos.Repository;
using System;
using System.Collections.Generic;

namespace SiteSerralheriaSantos.Models
{
    public class Usuario
    {
        #region Atributos
        public Guid UsuarioId { get; set; }
        public string Nome { get; set; }
        public string Login { get; set; }
        public string Senha { get; set; }
        public bool Ativo { get; set; }
        public TipoUsuario TipoUsuario { get; set; }
        #endregion

        #region Relacionamentos
        public Guid? EmpresaId { get; set; }
        public Empresa Empresa { get; set; }
        #endregion

        #region Crud
        UsuarioRepository _usuarioRepositories = new UsuarioRepository();
        public void Adicionar(Usuario usuario) => _usuarioRepositories.Adicionar(usuario);
        public void Atualizar(Usuario usuario) => _usuarioRepositories.Atualizar(usuario);
        public Usuario BuscarPorId(Guid usuarioId) => _usuarioRepositories.BuscarPorId(usuarioId);
        public Usuario BuscarPorLogin(string login) => _usuarioRepositories.BuscarPorLogin(login);
        public IEnumerable<Usuario> BuscarTodos() => _usuarioRepositories.BuscarTodos();
        public IEnumerable<Usuario> BuscarTodosPorNome(string textoDigitado) => _usuarioRepositories.BuscarTodosPorNome(textoDigitado);
        #endregion

        #region Métodos
        public void AtivarOuDesativar()
        {
            Ativo = !Ativo;
        }
        #endregion
    }
}