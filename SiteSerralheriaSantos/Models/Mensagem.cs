﻿using SiteSerralheriaSantos.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SiteSerralheriaSantos.Models
{
    public class Mensagem
    {
        #region Atributos
        public Guid MensagemId { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Texto { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DataDeEntrada { get; set; }
        public bool Visto { get; set; }
        #endregion

        #region Relacionamentos
        public Guid EmpresaId { get; set; }
        public Empresa Empresa { get; set; }
        #endregion

        #region Crud
        MensagemRepository _mensagemRepositorio = new MensagemRepository();
        public void Adicionar(Mensagem mensagem) => _mensagemRepositorio.Adicionar(mensagem);
        public void Atualizar(Mensagem mensagem) => _mensagemRepositorio.Atualizar(mensagem);
        public Mensagem BuscarPorId(Guid mensagemId) => _mensagemRepositorio.BuscarPorId(mensagemId);
        public IEnumerable<Mensagem> BuscarTodosPorData(DateTime data) => _mensagemRepositorio.BuscarTodosPorData(data);
        public IEnumerable<Mensagem> BuscarTodosPorMensagem(string textoDigitado) => _mensagemRepositorio.BuscarTodosPorMensagem(textoDigitado);
        public IEnumerable<Mensagem> BuscarTodos() => _mensagemRepositorio.BuscarTodos();
        public int QntDeMsgNaoVisualizada() => _mensagemRepositorio.QntDeMsgNaoVisualizada();
        #endregion
    }
}