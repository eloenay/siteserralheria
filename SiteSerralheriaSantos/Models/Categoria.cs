﻿using SiteSerralheriaSantos.Repository;
using System;
using System.Collections.Generic;

namespace SiteSerralheriaSantos.Models
{
    public class Categoria
    {
        #region Atributos
        public Guid CategoriaId { get; set; }
        public string Nome { get; set; }
        public bool Ativo { get; set; } = true;
        #endregion

        #region Bidirecional
        public ICollection<Galeria> Galerias { get; set; }
        #endregion

        #region Crud
        CategoriaRepository _categoriaRepositorio = new CategoriaRepository();
        public void Adicionar(Categoria categoria) => _categoriaRepositorio.Adicionar(categoria);
        public void Atualizar(Categoria categoria) => _categoriaRepositorio.Atualizar(categoria);
        public IEnumerable<Categoria> BuscarTodos() => _categoriaRepositorio.BuscarTodos();
        #endregion

        #region Métodos
        public void AtivarOuDesativar()
        {
            Ativo = !Ativo;
        }
        #endregion
    }
}