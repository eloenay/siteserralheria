﻿using SiteSerralheriaSantos.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SiteSerralheriaSantos.Models
{
    public class Empresa
    {
        #region Atributos
        public Guid EmpresaId { get; set; }
        public string Nome { get; set; }
        public string Celular { get; set; }
        public string CelularWhatsapp { get; set; }
        public string Telefone { get; set; }
        public string Email { get; set; }
        public string QuemSomos { get; set; }
        public string Informativo { get; set; }
        public string Missao { get; set; }
        public string Visao { get; set; }
        public string Valores { get; set; }
        public bool Ativo { get; set; } = true;

        #endregion

        #region Relacionamentos
        public Guid ImagemId { get; set; }
        public Imagem Imagem { get; set; }
        #endregion

        #region Bidirecional
        public ICollection<Endereco> Enderecos { get; set; }
        public ICollection<RedeSocial> RedesSociais { get; set; }
        public ICollection<Parceiro> Parceiros { get; set; }
        public ICollection<Galeria> Galerias { get; set; }
        public ICollection<Mensagem> Mensagens { get; set; }
        public ICollection<Equipe> Equipes { get; set; }
        public ICollection<Usuario> Usuarios { get; set; }
        #endregion

        #region Crud
        EmpresaRepository _empresaRepositorio = new EmpresaRepository();
        public void Adicionar(Empresa empresa) => _empresaRepositorio.Adicionar(empresa);
        public void Atualizar(Empresa empresa) => _empresaRepositorio.Atualizar(empresa);
        public Empresa BuscarPorId(Guid empresaId) => _empresaRepositorio.BuscarPorId(empresaId);
        public IEnumerable<Empresa> BuscarTodosPorEmpresa(string textoDigitado) => _empresaRepositorio.BuscarTodosPorEmpresa(textoDigitado);
        public IEnumerable<Empresa> BuscarTodos() => _empresaRepositorio.BuscarTodos();
        public Empresa BuscarEmpresaParaTelaInicial() => _empresaRepositorio.BuscarEmpresaParaTelaInicial();
        #endregion

        #region Métodos
        public void AtivarOuDesativar()
        {
            Ativo = !Ativo;
        }

        public string CelularWhatsappFormat => $"55{CelularWhatsapp}";
        #endregion
    }
}