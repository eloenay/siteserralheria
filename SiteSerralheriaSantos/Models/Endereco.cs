﻿using SiteSerralheriaSantos.Repository;
using System;
using System.Collections.Generic;

namespace SiteSerralheriaSantos.Models
{
    public class Endereco
    {
        #region Atributos
        public Guid EnderecoId { get; set; }
        public string Rua { get; set; }
        public string Bairro { get; set; }
        public string Cep { get; set; }
        public string Cidade { get; set; }
        public int Numero { get; set; }
        public string Estado { get; set; }
        public string Frame { get; set; }
        public bool Ativo { get; set; } = true;
        #endregion

        #region Relacionamentos
        public Guid EmpresaId { get; set; }
        public Empresa Empresa { get; set; }
        #endregion

        #region Crud
        EnderecoRepository _enderecoRepositorio = new EnderecoRepository();
        public void Adicionar(Endereco endereco) => _enderecoRepositorio.Adicionar(endereco);
        public void Atualizar(Endereco endereco) => _enderecoRepositorio.Atualizar(endereco);
        public Endereco BuscarPorId(Guid enderecoId) => _enderecoRepositorio.BuscarPorId(enderecoId);
        public IEnumerable<Endereco> BuscarTodosEnderecoPorEmpresa(Guid empresaId) => _enderecoRepositorio.BuscarTodosEnderecoPorEmpresa(empresaId);

        #endregion

        #region Métodos
        public void AtivarOuDesativar()
        {
            Ativo = !Ativo;
        }

        public string FormatEndereco => $"{Rua} - {Numero}, {Bairro} - {Cidade}";
        #endregion
    }
}