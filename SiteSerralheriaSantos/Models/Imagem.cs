﻿using SiteSerralheriaSantos.Repository;
using System;
using System.Collections.Generic;
using System.IO;

namespace SiteSerralheriaSantos.Models
{
    public class Imagem
    {
        #region Atributos
        public Guid ImagemId { get; set; }
        public string Url { get; set; }
        #endregion

        #region Bidirecional
        public ICollection<Galeria> Galerias { get; set; }
        public ICollection<Empresa> Empresas { get; set; } 
        public ICollection<Parceiro> Parceiros { get; set; }
        public ICollection<Equipe> Equipes { get; set; }
        #endregion

        #region Crud
        ImagemRepository _imagemRepositorio = new ImagemRepository();
        public void Adicionar(Imagem imagem) => _imagemRepositorio.Adicionar(imagem);
        public void Atualizar(Imagem imagem) => _imagemRepositorio.Atualizar(imagem);
        public Imagem BuscarPorId(Guid imagemId) => _imagemRepositorio.BuscarPorId(imagemId);
        #endregion

        #region Métodos
        public string CaminhoImgGaleria(string imgGaleria)
        {
            return string.Format("../../Uploads/galeria/{0}", imgGaleria);
        }

        public string CaminhoImgParceiro(string imgParceiro)
        {
            return string.Format("../../Uploads/parceiro/{0}", imgParceiro);
        }

        public string CaminhoImgLogoEmpresa(string imgLogo)
        {
            return string.Format("../../Uploads/logo/{0}", imgLogo);
        }

        public string CaminhoImgEquipe(string imgEquipe)
        {
            return string.Format("../../Uploads/equipe/{0}", imgEquipe);
        }
        #endregion

        #region Não Mapeados
        public string NomeImagemGaleria => Path.GetFileNameWithoutExtension(Url);
        public string NomeImagemParceiro => Path.GetFileNameWithoutExtension(Url);
        public string NomeImagemLogoEmpresa => Path.GetFileNameWithoutExtension(Url);
        public string NomeImagemEquipe => Path.GetFileNameWithoutExtension(Url);
        #endregion
    }
}