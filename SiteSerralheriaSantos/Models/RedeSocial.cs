﻿using SiteSerralheriaSantos.Repository;
using System;
using System.Collections.Generic;

namespace SiteSerralheriaSantos.Models
{
    public class RedeSocial
    {
        #region Atributos
        public Guid RedeSocialId { get; set; }
        public TipoRedeSocial TipoRedeSocial { get; set; }
        public string Link { get; set; }
        public bool Ativo { get; set; } = true;
        #endregion

        #region Relacionamentos
        public Guid EmpresaId { get; set; }
        public Empresa Empresa { get; set; }
        #endregion

        #region Crud
        RedeSocialRepository _redeSocialRepositorio = new RedeSocialRepository();
        public void Adicionar(RedeSocial redesocial) => _redeSocialRepositorio.Adicionar(redesocial);
        public void Atualizar(RedeSocial redesocial) => _redeSocialRepositorio.Atualizar(redesocial);
        public RedeSocial BuscarPorId(Guid redesocialId) => _redeSocialRepositorio.BuscarPorId(redesocialId);
        public IEnumerable<RedeSocial> BuscarTodosRedeSocialPorEmpresa(Guid empresaId) => _redeSocialRepositorio.BuscarTodosRedeSocialPorEmpresa(empresaId);

        #endregion

        #region Métodos
        public void AtivarOuDesativar()
        {
            Ativo = !Ativo;
        }
        #endregion
    }
}