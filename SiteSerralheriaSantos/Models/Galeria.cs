﻿using SiteSerralheriaSantos.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SiteSerralheriaSantos.Models
{
    public class Galeria
    {
        #region Atributos
        public Guid GaleriaId { get; set; }

        [Required]
        [Display(Name = "Título *")]
        public string Titulo { get; set; }

        public bool Ativo { get; set; } = true;
        #endregion

        #region Relacionamentos
        public Guid ImagemId { get; set; }
        public Imagem Imagem { get; set; }
        public Guid EmpresaId { get; set; }
        public Empresa Empresa { get; set; }
        public Guid CategoriaId { get; set; }
        public Categoria Categoria { get; set; }
        #endregion

        #region Crud
        GaleriaRepository _galeriaRepositorio = new GaleriaRepository();
        public void Adicionar(Galeria galeria) => _galeriaRepositorio.Adicionar(galeria);
        public void Atualizar(Galeria galeria) => _galeriaRepositorio.Atualizar(galeria);
        public Galeria BuscarPorId(Guid galeriaId) => _galeriaRepositorio.BuscarPorId(galeriaId);
        public IEnumerable<Galeria> BuscarTodosPorGaleria(string textoDigitado) => _galeriaRepositorio.BuscarTodosPorGaleria(textoDigitado);
        public IEnumerable<Galeria> BuscarTodos() => _galeriaRepositorio.BuscarTodos();
        public IEnumerable<Galeria> BuscarTodosPorEmpresa(Guid empresaId) => _galeriaRepositorio.BuscarTodosPorEmpresa(empresaId);
        #endregion

        #region Métodos
        public void AtivarOuDesativar()
        {
            Ativo = !Ativo;
        }
        #endregion
    }
}