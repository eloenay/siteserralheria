﻿using SiteSerralheriaSantos.Repository;
using System;
using System.Collections.Generic;

namespace SiteSerralheriaSantos.Models
{
    public class Parceiro
    {
        #region Atributos
        public Guid ParceiroId { get; set; }
        public string Nome { get; set; }
        public string Link { get; set; }
        public bool Ativo { get; set; } = true;
        #endregion

        #region Relacionamentos
        public Guid EmpresaId { get; set; }
        public Empresa Empresa { get; set; }
        public Guid ImagemId { get; set; }
        public Imagem Imagem { get; set; }
        #endregion

        #region Crud
        ParceiroRepository _parceiroRepositorio = new ParceiroRepository();
        public void Adicionar(Parceiro parceiro) => _parceiroRepositorio.Adicionar(parceiro);
        public void Atualizar(Parceiro parceiro) => _parceiroRepositorio.Atualizar(parceiro);
        public Parceiro BuscarPorId(Guid parceiroId) => _parceiroRepositorio.BuscarPorId(parceiroId);
        public IEnumerable<Parceiro> BuscarTodos() => _parceiroRepositorio.BuscarTodos();
        public IEnumerable<Parceiro> BuscarTodosPorParceiro(string textoDigitado) => _parceiroRepositorio.BuscarTodosPorParceiro(textoDigitado);
        public IEnumerable<Parceiro> BuscarTodosPorEmpresa(Guid empresaId) => _parceiroRepositorio.BuscarTodosPorEmpresa(empresaId);
        #endregion

        #region Métodos
        public void AtivarOuDesativar()
        {
            Ativo = !Ativo;
        }
        #endregion
    }
}