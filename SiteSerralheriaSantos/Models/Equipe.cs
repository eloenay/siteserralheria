﻿using SiteSerralheriaSantos.Repository;
using System;
using System.Collections.Generic;

namespace SiteSerralheriaSantos.Models
{
    public class Equipe
    {
        #region Atributos
        public Guid EquipeId { get; set; }
        public string Nome { get; set; }
        public string Cargo { get; set; }
        public bool Ativo { get; set; } = true;
        #endregion

        #region Relacionamentos
        public Guid EmpresaId { get; set; }
        public Empresa Empresa { get; set; }
        public Guid ImagemId { get; set; }
        public Imagem Imagem { get; set; }
        #endregion

        #region Crud
        EquipeRepository _equipeRepositorio = new EquipeRepository();
        public void Adicionar(Equipe equipe) => _equipeRepositorio.Adicionar(equipe);
        public void Atualizar(Equipe equipe) => _equipeRepositorio.Atualizar(equipe);
        public Equipe BuscarPorId(Guid equipeId) => _equipeRepositorio.BuscarPorId(equipeId);
        public IEnumerable<Equipe> BuscarTodosPorEquipe(string textoDigitado) => _equipeRepositorio.BuscarTodosPorEquipe(textoDigitado);
        public IEnumerable<Equipe> BuscarTodos() => _equipeRepositorio.BuscarTodos();
        #endregion

        #region Métodos
        public void AtivarOuDesativar()
        {
            Ativo = !Ativo;
        }
        #endregion
    }
}