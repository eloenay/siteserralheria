﻿using System.ComponentModel.DataAnnotations;

namespace SiteSerralheriaSantos.Models
{
    public enum TipoUsuario
    {
        Master = 1,
        Administrador = 2,
        [Display(Name = "Usuário")]
        Usuario = 3
    }

    public enum TipoRedeSocial
    {
        facebook = 1,
        twitter = 2,
        google = 3,
        linkedin = 4,
        youtube = 5,
        instagram = 6
    }
}