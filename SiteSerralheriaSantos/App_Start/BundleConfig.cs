﻿using System.Web.Optimization;

namespace SiteSerralheriaSantos
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region JS
            //Layout interno
            bundles.Add(new ScriptBundle("~/bundles/interno").Include(
                      "~/Scripts/frameworks/jquery-{version}.js",
                      "~/Scripts/frameworks/jquery-jquery.unobtrusive-ajax.js",
                      "~/Scripts/frameworks/jquery-migrate-3.0.1.min.js",
                      "~/Scripts/frameworks/jquery-ui.js",
                      "~/Scripts/frameworks/bootstrap.js",
                      "~/Scripts/frameworks/custom.js"));

            //Layout externo
            bundles.Add(new ScriptBundle("~/bundles/externo").Include(
                      "~/Scripts/frameworks/jquery-{version}.js",
                      "~/Scripts/frameworks/jquery-jquery.unobtrusive-ajax.js",
                      "~/Scripts/frameworks/jquery-migrate-3.0.1.min.js",
                      "~/Scripts/frameworks/popper.min.js",
                      "~/Scripts/frameworks/bootstrap.min.js",
                      "~/Scripts/frameworks/easing.js",
                      "~/Scripts/frameworks/mobile-nav.js",
                      "~/Scripts/frameworks/wow.js",
                      "~/Scripts/frameworks/waypoints.min.js",
                      "~/Scripts/frameworks/counterup.min.js",
                      "~/Scripts/frameworks/owl.carousel.min.js",
                      "~/Scripts/frameworks/isotope.pkgd.js",
                      "~/Scripts/frameworks/lightbox.js",
                      "~/Scripts/frameworks/contactform.js",
                      "~/Scripts/site/main.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include("~/Scripts/frameworks/jquery.validate*"));
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include("~/Scripts/frameworks/modernizr-*"));
            bundles.Add(new ScriptBundle("~/bundles/notificacao").Include("~/Scripts/frameworks/sweetalert2.js", "~/Scripts/frameworks/toastr.js", "~/Scripts/site/script-notificar.js"));
            bundles.Add(new ScriptBundle("~/bundles/carregar-modal").Include("~/Scripts/site/carregar-modal.js"));
            bundles.Add(new ScriptBundle("~/bundles/carregar-imagem").Include("~/Scripts/site/carregar-imagem.js"));
            bundles.Add(new ScriptBundle("~/bundles/buscar-paginacao").Include("~/Scripts/site/buscar-paginacao.js"));
            bundles.Add(new ScriptBundle("~/bundles/evento-ativo").Include("~/Scripts/site/evento-ativo.js"));
            bundles.Add(new ScriptBundle("~/bundles/add-or-update-redesocial").Include("~/Scripts/site/add-or-update-redesocial.js"));
            bundles.Add(new ScriptBundle("~/bundles/add-or-update-endereco").Include("~/Scripts/site/add-or-update-endereco.js"));
            bundles.Add(new ScriptBundle("~/bundles/add-categoria").Include("~/Scripts/site/add-categoria.js"));
            bundles.Add(new ScriptBundle("~/bundles/mask").Include(
                "~/Scripts/frameworks/jquery.mask.js",
                "~/Scripts/frameworks/jquery.priceformat.js",
                "~/Scripts/site/mask.js"));
            #endregion

            #region CSS
            //Layout interno
            bundles.Add(new StyleBundle("~/Content/interno").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/font-awesome.css",
                      "~/Content/custom.css",
                      "~/Content/site.css"));

            //Layout externo
            bundles.Add(new StyleBundle("~/Content/externo").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/font-awesome.css",
                      "~/Content/animate.css",
                      "~/Content/ionicons.css",
                      "~/Content/owl.carousel.min.css",
                      "~/Content/lightbox.css",
                      "~/Content/style.css"));

            bundles.Add(new StyleBundle("~/Content/site").Include("~/Content/interno.css"));
            bundles.Add(new StyleBundle("~/Content/carregar-imagem").Include("~/Content/carregar-imagem.css"));
            bundles.Add(new StyleBundle("~/Content/notificacao").Include("~/Content/sweetalert2.css", "~/Content/toastr.css")); 
            #endregion
        }
    }
}
