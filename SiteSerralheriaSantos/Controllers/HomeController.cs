﻿using SiteSerralheriaSantos.Help;
using SiteSerralheriaSantos.Models;
using System;
using System.Web.Mvc;

namespace SiteSerralheriaSantos.Controllers
{
    public class HomeController : Controller
    {
        private readonly Empresa _empresa;
        private readonly Mensagem _mensagem;
        private readonly Galeria _galeria;
        private readonly Parceiro _parceiro;

        public HomeController()
        {
            _empresa = new Empresa();
            _mensagem = new Mensagem();
            _galeria = new Galeria();
            _parceiro = new Parceiro();
        }

        public ActionResult Index()
        {
            var empresa = _empresa.BuscarEmpresaParaTelaInicial();
            ViewBag.Empresa = (empresa == null) ? _empresa : empresa;
            return View();
        }

        public ActionResult Projetos()
        {
            var empresa = _empresa.BuscarEmpresaParaTelaInicial();
            ViewBag.Empresa = (empresa == null) ? _empresa : empresa;
            var t = _galeria.BuscarTodosPorEmpresa(empresa.EmpresaId);
            return View(_galeria.BuscarTodosPorEmpresa(empresa.EmpresaId));
        }

        public ActionResult SobreNos()
        {
            var empresa = _empresa.BuscarEmpresaParaTelaInicial();
            ViewBag.Empresa = (empresa == null) ? _empresa : empresa;
            return View(empresa);
        }

        public ActionResult Parceiros()
        {
            var empresa = _empresa.BuscarEmpresaParaTelaInicial();
            ViewBag.Empresa = (empresa == null) ? _empresa : empresa;
            return View(_parceiro.BuscarTodosPorEmpresa(empresa.EmpresaId));
        }

        public ActionResult AbrirModalLogin()
        {
            return PartialView("_ModalLogin");
        }

        [HttpPost]
        public ActionResult AdicionarMensagem(Mensagem mensagem)
        {
            if (!ModelState.IsValid) return RedirectToAction("Index", "Home").Error("Erro ao enviar mensagem!");
            mensagem.DataDeEntrada = DateTime.Now.Date;
            mensagem.Visto = false;
            mensagem.EmpresaId = _empresa.BuscarEmpresaParaTelaInicial().EmpresaId;
            _mensagem.Adicionar(mensagem);
            return RedirectToAction("Index", "Home").Success("Mensagem enviada com sucesso!");
        }
    }
}