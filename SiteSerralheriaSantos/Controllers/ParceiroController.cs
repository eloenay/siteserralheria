﻿using PagedList;
using SiteSerralheriaSantos.Help;
using SiteSerralheriaSantos.Models;
using SiteSerralheriaSantos.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;

namespace SiteSerralheriaSantos.Controllers
{
    public class ParceiroController : Controller
    {
        private readonly Parceiro _parceiro;
        private readonly Imagem _imagem;
        private readonly Usuario _usuario;
        private readonly Empresa _empresa;
        private readonly Mensagem _mensagem;

        public ParceiroController()
        {
            _parceiro = new Parceiro();
            _imagem = new Imagem();
            _usuario = new Usuario();
            _empresa = new Empresa();
            _mensagem = new Mensagem();
        }

        public ActionResult Index(int pagina = 1)
        {
            ViewBag.EmpresaId = _usuario.BuscarPorId(Account.UsuarioId).EmpresaId;
            ViewBag.Mensagem = _mensagem.QntDeMsgNaoVisualizada();
            TempData["Pagina"] = pagina;
            int itensPorPagina = 8;
            var parceiros = _parceiro.BuscarTodos();
            var lista = new List<Parceiro>();
            lista.AddRange(parceiros);
            return View(lista.ToPagedList(pagina, itensPorPagina));
        }

        public ActionResult Adicionar()
        {
            ViewBag.Empresas = new SelectList(_empresa.BuscarTodos(), "EmpresaId", "Nome");
            ViewBag.Mensagem = _mensagem.QntDeMsgNaoVisualizada();
            return View();
        }

        [HttpPost]
        public ActionResult Adicionar(Parceiro parceiro, HttpPostedFileBase fotoParceiro)
        {
            if (!ModelState.IsValid) return View(parceiro).Error("Erro ao cadastrar parceiro");
            if (fotoParceiro != null)
            {
                var nomeDaImgParceiro = Path.GetFileName(fotoParceiro.FileName);
                var pathParceiro = Path.Combine(Server.MapPath("~/Uploads/parceiro/"), nomeDaImgParceiro);
                fotoParceiro.SaveAs(pathParceiro);
                var imagem = new Imagem();
                imagem.Url = imagem.CaminhoImgParceiro(fotoParceiro.FileName);
                imagem.ImagemId = Guid.NewGuid();
                _imagem.Adicionar(imagem);
                parceiro.ImagemId = imagem.ImagemId;
                var entId = _usuario.BuscarPorId(Account.UsuarioId).EmpresaId;
                parceiro.EmpresaId = (entId == Guid.Empty || entId == null) ? parceiro.EmpresaId : (Guid)entId;
                _parceiro.Adicionar(parceiro);
                return RedirectToAction("Index").Success("Parceiro cadastrado com sucesso");
            }
            else
            {
                return View(parceiro).Error("Erro ao cadastrar parceiro");
            }
        }

        public ActionResult Atualizar(Guid parceiroId)
        {
            ViewBag.Mensagem = _mensagem.QntDeMsgNaoVisualizada();
            return View(_parceiro.BuscarPorId(parceiroId));
        }

        [HttpPost]
        public ActionResult Atualizar(Parceiro parceiro, HttpPostedFileBase fotoParceiro)
        {
            if (!ModelState.IsValid) return View(parceiro).Error("Erro ao atualizar parceiro");
            if (fotoParceiro == null)
            {
                _parceiro.Atualizar(parceiro);
                return RedirectToAction("Atualizar", new { parceiroId = parceiro.ParceiroId }).Success("Parceiro atualizado com sucesso");
            }
            else
            {
                var nomeDaImgParceiro = Path.GetFileName(fotoParceiro.FileName);
                var pathParcero = Path.Combine(Server.MapPath("~/Uploads/parceiro/"), nomeDaImgParceiro);
                fotoParceiro.SaveAs(pathParcero);
                var imagem = _imagem.BuscarPorId(parceiro.ImagemId);
                imagem.Url = imagem.CaminhoImgParceiro(fotoParceiro.FileName);
                _imagem.Atualizar(imagem);
                _parceiro.Atualizar(parceiro);
                return RedirectToAction("Atualizar", new { parceiroId = parceiro.ParceiroId }).Success("Parceiro atualizado com sucesso");
            }
        }

        public ActionResult Ativo(Guid parceiroId, int pagina)
        {
            var parceiroAtual = _parceiro.BuscarPorId(parceiroId);
            parceiroAtual.AtivarOuDesativar();
            _parceiro.Atualizar(parceiroAtual);
            TempData["Pagina"] = pagina;
            int itensPorPagina = 8;
            var parceiros = _parceiro.BuscarTodos();
            var lista = new List<Parceiro>();
            lista.AddRange(parceiros);
            return PartialView("_PartialTabela", lista.ToPagedList(pagina, itensPorPagina));
        }

        public PartialViewResult BuscarParceiro(string textoDigitado, int pagina = 1)
        {
            TempData["Pagina"] = pagina;
            int itensPorPagina = 8;
            var parceiros = _parceiro.BuscarTodosPorParceiro(textoDigitado);
            var lista = new List<Parceiro>();
            lista.AddRange(parceiros);
            return PartialView("_PartialTabela", lista.ToPagedList(pagina, itensPorPagina));
        }
    }
}