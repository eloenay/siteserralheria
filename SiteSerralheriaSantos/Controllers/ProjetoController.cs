﻿using PagedList;
using SiteSerralheriaSantos.Help;
using SiteSerralheriaSantos.Models;
using SiteSerralheriaSantos.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;

namespace SiteSerralheriaSantos.Controllers
{
    public class ProjetoController : Controller
    {
        private readonly Galeria _galeria;
        private readonly Imagem _imagem;
        private readonly Categoria _categoria;
        private readonly Usuario _usuario;
        private readonly Empresa _empresa;
        private readonly Mensagem _mensagem;

        public ProjetoController()
        {
            _galeria = new Galeria();
            _imagem = new Imagem();
            _categoria = new Categoria();
            _usuario = new Usuario();
            _empresa = new Empresa();
            _mensagem = new Mensagem();
        }

        public ActionResult Index(int pagina = 1)
        {
            ViewBag.EmpresaId = _usuario.BuscarPorId(Account.UsuarioId).EmpresaId;
            ViewBag.Mensagem = _mensagem.QntDeMsgNaoVisualizada();
            TempData["Pagina"] = pagina;
            int itensPorProjeto = 8;
            var projetos = _galeria.BuscarTodos();
            var lista = new List<Galeria>();
            lista.AddRange(projetos);
            return View(lista.ToPagedList(pagina, itensPorProjeto));
        }

        public ActionResult Adicionar()
        {
            ViewBag.Categorias = new SelectList(_categoria.BuscarTodos(), "CategoriaId", "Nome");
            ViewBag.Empresas = new SelectList(_empresa.BuscarTodos(), "EmpresaId", "Nome");
            ViewBag.Mensagem = _mensagem.QntDeMsgNaoVisualizada();
            return View();
        }

        [HttpPost]
        public ActionResult Adicionar(Galeria galeria, HttpPostedFileBase fotoProjeto)
        {
            if (!ModelState.IsValid) return View(galeria).Error();
            if (fotoProjeto != null)
            {
                var nomeDaImgProjeto = Path.GetFileName(fotoProjeto.FileName);
                var pathProjeto = Path.Combine(Server.MapPath("~/Uploads/galeria/"), nomeDaImgProjeto);
                fotoProjeto.SaveAs(pathProjeto);
                var imagem = new Imagem();
                imagem.Url = imagem.CaminhoImgGaleria(fotoProjeto.FileName);
                imagem.ImagemId = Guid.NewGuid();
                _imagem.Adicionar(imagem);
                galeria.ImagemId = imagem.ImagemId;
                var entId = _usuario.BuscarPorId(Account.UsuarioId).EmpresaId;
                galeria.EmpresaId = (entId == Guid.Empty || entId == null) ? galeria.EmpresaId : (Guid)entId;
                _galeria.Adicionar(galeria);
                return RedirectToAction("Index").Success();
            }
            else
            {
                return View(galeria).Error();
            }
        }

        public ActionResult Atualizar(Guid projetoId)
        {
            ViewBag.Mensagem = _mensagem.QntDeMsgNaoVisualizada();
            ViewBag.Categorias = new SelectList(_categoria.BuscarTodos(), "CategoriaId", "Nome");
            return View(_galeria.BuscarPorId(projetoId));
        }

        [HttpPost]
        public ActionResult Atualizar(Galeria galeria, HttpPostedFileBase fotoProjeto)
        {
            if (!ModelState.IsValid) return View(galeria).Error();
            if (fotoProjeto == null)
            {
                _galeria.Atualizar(galeria);
                return RedirectToAction("Atualizar", new { projetoId = galeria.GaleriaId }).Success();
            }
            else
            {
                var nomeDaImgProjeto = Path.GetFileName(fotoProjeto.FileName);
                var pathProjeto = Path.Combine(Server.MapPath("~/Uploads/galeria/"), nomeDaImgProjeto);
                fotoProjeto.SaveAs(pathProjeto);
                var imagem = _imagem.BuscarPorId(galeria.ImagemId);
                imagem.Url = imagem.CaminhoImgGaleria(fotoProjeto.FileName);
                _imagem.Atualizar(imagem);
                _galeria.Atualizar(galeria);
                return RedirectToAction("Atualizar", new { projetoId = galeria.GaleriaId }).Success();
            }
        }

        public ActionResult Ativo(Guid projetoId, int pagina)
        {
            var projetoAtual = _galeria.BuscarPorId(projetoId);
            projetoAtual.AtivarOuDesativar();
            _galeria.Atualizar(projetoAtual);
            TempData["Pagina"] = pagina;
            int itensPorPagina = 8;
            var projetos = _galeria.BuscarTodos();
            var lista = new List<Galeria>();
            lista.AddRange(projetos);
            return PartialView("_PartialTabela", lista.ToPagedList(pagina, itensPorPagina));
        }

        public PartialViewResult BuscarProjeto(string textoDigitado, int pagina = 1)
        {
            TempData["Pagina"] = pagina;
            int itensPorPagina = 8;
            var projetos = _galeria.BuscarTodosPorGaleria(textoDigitado);
            var lista = new List<Galeria>();
            lista.AddRange(projetos);
            return PartialView("_PartialTabela", lista.ToPagedList(pagina, itensPorPagina));
        }

        public ActionResult AbrirModalAdicionarCategoria()
        {
            return PartialView("_ModalAdicionarCategoria");
        }

        [HttpPost]
        public void AdicionarCategoria(Categoria categoria)
        {
            _categoria.Adicionar(categoria);
        }
    }
}