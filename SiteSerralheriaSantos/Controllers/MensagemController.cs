﻿using PagedList;
using SiteSerralheriaSantos.Models;
using SiteSerralheriaSantos.ViewModels;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace SiteSerralheriaSantos.Controllers
{
    public class MensagemController : Controller
    {
        private readonly Mensagem _mensagem;
        private readonly Empresa _empresa;
        private readonly Usuario _usuario;

        public MensagemController()
        {
            _mensagem = new Mensagem();
            _empresa = new Empresa();
            _usuario = new Usuario();
        }

        public ActionResult Index(int pagina = 1)
        {
            ViewBag.EmpresaId = _usuario.BuscarPorId(Account.UsuarioId).EmpresaId;
            ViewBag.Mensagem = _mensagem.QntDeMsgNaoVisualizada();
            TempData["Pagina"] = pagina;
            int itensPorPagina = 8;
            var mensagens = _mensagem.BuscarTodos();
            var lista = new List<Mensagem>();
            lista.AddRange(mensagens);
            return View(lista.ToPagedList(pagina, itensPorPagina));
        }

        public ActionResult Descricao(Guid mensagemId)
        {
            var mensagem = _mensagem.BuscarPorId(mensagemId);
            mensagem.Visto = true;
            _mensagem.Atualizar(mensagem);
            ViewBag.Mensagem = _mensagem.QntDeMsgNaoVisualizada();
            return View(mensagem);
        }

        public PartialViewResult BuscarMensagem(string textoDigitado, int pagina = 1)
        {
            TempData["Pagina"] = pagina;
            int itensPorPagina = 8;
            var mensagens = _mensagem.BuscarTodosPorMensagem(textoDigitado);
            var lista = new List<Mensagem>();
            lista.AddRange(mensagens);
            return PartialView("_PartialTabela", lista.ToPagedList(pagina, itensPorPagina));
        }
    }
}