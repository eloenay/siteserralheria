﻿using PagedList;
using SiteSerralheriaSantos.Help;
using SiteSerralheriaSantos.Models;
using SiteSerralheriaSantos.Utils;
using SiteSerralheriaSantos.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace SiteSerralheriaSantos.Controllers
{
    public class LoginController : Controller
    {
        private readonly Usuario _usuario;
        private readonly Empresa _empresa;
        private readonly Mensagem _mensagem;
        
        public LoginController()
        {
            _usuario = new Usuario();
            _empresa = new Empresa();
            _mensagem = new Mensagem();
        }

        [Authorize]
        public ActionResult Index(int pagina = 1)
        {
            ViewBag.EmpresaId = _usuario.BuscarPorId(Account.UsuarioId).EmpresaId;
            ViewBag.Mensagem = _mensagem.QntDeMsgNaoVisualizada();
            TempData["Pagina"] = pagina;
            int itensPorPagina = 5;
            var membrosDaEquipes = _usuario.BuscarTodos();
            var lista = new List<Usuario>();
            lista.AddRange(membrosDaEquipes);
            return View(lista.ToPagedList(pagina, itensPorPagina));
        }

        [Authorize]
        public ActionResult Adicionar()
        {
            ViewBag.Empresas = new SelectList(_empresa.BuscarTodos(), "EmpresaId", "Nome");
            ViewBag.Mensagem = _mensagem.QntDeMsgNaoVisualizada();
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Adicionar(CadastrarUsuarioVm usuario)
        {
            if (!ModelState.IsValid) return View(usuario).Error("Erro ao cadastrar usuário!");
            if (_usuario.BuscarTodos().Count(u => u.Login == usuario.Login) > 0) return View(usuario).Error("Esse login já está em uso");
            var usu = _usuario.BuscarPorId(Account.UsuarioId).EmpresaId;
            var empresaId = (usu == Guid.Empty || usu == null) ? usuario.EmpresaId : usu;
            Usuario novoUsuario = new Usuario
            {
                Nome = usuario.Nome,
                Login = usuario.Login,
                Senha = Hash.GerarHash(usuario.Senha),
                EmpresaId = empresaId,
                Ativo = true,
                TipoUsuario = usuario.TipoUsuario
            };
            _usuario.Adicionar(novoUsuario);
            return RedirectToAction("Index").Success("Usuário cadastrado com sucesso!");
        }

        public ActionResult Atualizar(Guid usuarioId)
        {
            ViewBag.Mensagem = _mensagem.QntDeMsgNaoVisualizada();
            return View(_usuario.BuscarPorId(usuarioId));
        }

        [HttpPost]
        public ActionResult Atualizar(Usuario usuario)
        {
            if (!ModelState.IsValid) return View().Error("Erro ao atualizar usuário");
            _usuario.Atualizar(usuario);
            return RedirectToAction("Index").Success("Sistema atualizado com sucesso");
        }

        [HttpPost]
        public ActionResult Entrar(LoginVm loginVm)
        {
            if (!ModelState.IsValid) return RedirectToAction("Index", "Home").Error("Erro ao fazer login!");
            if (loginVm == null) return RedirectToAction("Index", "Home").Error("Preencha todos os campos!");
            var usuario = _usuario.BuscarPorLogin(loginVm.Login);
            if (usuario == null) return RedirectToAction("Index", "Home").Error("Este usuário não existe!");
            if (usuario.Ativo != true) return RedirectToAction("Index", "Home").Error("Este usuário está desativado!");
            if (usuario.Senha != Hash.GerarHash(loginVm.Senha)) return RedirectToAction("Index", "Home").Error("Senha incorreta!");
            var identity = new ClaimsIdentity(new[]
            {
                new Claim("http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider","ASP.NET Identity"),
                new Claim(ClaimTypes.NameIdentifier, usuario.UsuarioId.ToString()),
                new Claim(ClaimTypes.Name, usuario.Nome),
                new Claim(ClaimTypes.Role, usuario.TipoUsuario.ToString()),
            }, "ApplicationCookie");
            Request.GetOwinContext().Authentication.SignIn(identity);

            if (!String.IsNullOrWhiteSpace(loginVm.UrlRetorno) || Url.IsLocalUrl(loginVm.UrlRetorno)) return Redirect(loginVm.UrlRetorno);

            if (usuario.Empresa == null && (usuario.TipoUsuario == TipoUsuario.Administrador || usuario.TipoUsuario == TipoUsuario.Usuario))
                return RedirectToAction("Adicionar", "Empresa").Information($"Bem vindo {usuario.Nome}");
            else if (usuario.Empresa != null && (usuario.TipoUsuario == TipoUsuario.Administrador || usuario.TipoUsuario == TipoUsuario.Usuario))
                return RedirectToAction("Atualizar", "Empresa", new { empresaId = usuario.Empresa.EmpresaId }).Information($"Bem vindo {usuario.Nome}");
            else
                return RedirectToAction("Index", "Empresa").Information($"Bem vindo {usuario.Nome}");
        }

        public ActionResult Logout()
        {
            Request.GetOwinContext().Authentication.SignOut("ApplicationCookie");
            return RedirectToAction("Index", "Home");
        }

        [Authorize]
        public ActionResult AlterarSenha()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult AlterarSenha(AlterarSenhaVm altrSenha)
        {
            if (!ModelState.IsValid) return View(altrSenha).Error("Erro ao alterar senha!");
            var usuario = _usuario.BuscarPorId(Account.UsuarioId);
            usuario.Senha = Hash.GerarHash(altrSenha.NovaSenha);
            _usuario.Atualizar(usuario);
            return View().Success("Senha atualizada com sucesso!");
        }

        public ActionResult Ativo(Guid usuarioId, int pagina)
        {
            var usuarioAtual = _usuario.BuscarPorId(usuarioId);
            usuarioAtual.AtivarOuDesativar();
            _usuario.Atualizar(usuarioAtual);
            TempData["Pagina"] = pagina;
            int itensPorPagina = 5;
            var usuarios = _usuario.BuscarTodos();
            var lista = new List<Usuario>();
            lista.AddRange(usuarios);
            return PartialView("_PartialTabela", lista.ToPagedList(pagina, itensPorPagina));
        }

        public PartialViewResult BuscarUsuario(string textoDigitado, int pagina = 1)
        {
            TempData["Pagina"] = pagina;
            int itensPorPagina = 5;
            var usuarios = _usuario.BuscarTodosPorNome(textoDigitado);
            var lista = new List<Usuario>();
            lista.AddRange(usuarios);
            return PartialView("_PartialTabela", lista.ToPagedList(pagina, itensPorPagina));
        }
    }
}