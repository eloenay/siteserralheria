﻿using PagedList;
using SiteSerralheriaSantos.Help;
using SiteSerralheriaSantos.Models;
using SiteSerralheriaSantos.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;

namespace SiteSerralheriaSantos.Controllers
{
    public class EquipeController : Controller
    {
        private readonly Equipe _equipe;
        private readonly Imagem _imagem;
        private readonly Usuario _usuario;
        private readonly Empresa _empresa;
        private readonly Mensagem _mensagem;

        public EquipeController()
        {
            _equipe = new Equipe();
            _imagem = new Imagem();
            _usuario = new Usuario();
            _empresa = new Empresa();
            _mensagem = new Mensagem();
        }

        public ActionResult Index(int pagina = 1)
        {
            ViewBag.EmpresaId = _usuario.BuscarPorId(Account.UsuarioId).EmpresaId;
            ViewBag.Mensagem = _mensagem.QntDeMsgNaoVisualizada();
            TempData["Pagina"] = pagina;
            int itensPorEquipe = 8;
            var equipes = _equipe.BuscarTodos();
            var lista = new List<Equipe>();
            lista.AddRange(equipes);
            return View(lista.ToPagedList(pagina, itensPorEquipe));
        }

        public ActionResult Adicionar()
        {
            ViewBag.Empresas = new SelectList(_empresa.BuscarTodos(), "EmpresaId", "Nome");
            ViewBag.Mensagem = _mensagem.QntDeMsgNaoVisualizada();
            return View();
        }

        [HttpPost]
        public ActionResult Adicionar(Equipe equipe, HttpPostedFileBase fotoEquipe)
        {
            if (!ModelState.IsValid) return View(equipe).Error();
            if (fotoEquipe != null)
            {
                var nomeDaImgEquipe = Path.GetFileName(fotoEquipe.FileName);
                var pathEquipe = Path.Combine(Server.MapPath("~/Uploads/equipe/"), nomeDaImgEquipe);
                fotoEquipe.SaveAs(pathEquipe);
                var imagem = new Imagem();
                imagem.Url = imagem.CaminhoImgEquipe(fotoEquipe.FileName);
                imagem.ImagemId = Guid.NewGuid();
                _imagem.Adicionar(imagem);
                equipe.ImagemId = imagem.ImagemId;
                var entId = _usuario.BuscarPorId(Account.UsuarioId).EmpresaId;
                equipe.EmpresaId = (entId == Guid.Empty || entId == null) ? equipe.EmpresaId : (Guid)entId;
                _equipe.Adicionar(equipe);
                return RedirectToAction("Index").Success("Integrante da equipe adicionado com sucesso.");
            }
            else
            {
                return View(equipe).Error();
            }
        }

        public ActionResult Atualizar(Guid equipeId)
        {
            ViewBag.Mensagem = _mensagem.QntDeMsgNaoVisualizada();
            return View(_equipe.BuscarPorId(equipeId));
        }

        [HttpPost]
        public ActionResult Atualizar(Equipe equipe, HttpPostedFileBase fotoEquipe)
        {
            if (!ModelState.IsValid) return View(equipe).Error();
            if (fotoEquipe == null)
            {
                _equipe.Atualizar(equipe);
                return RedirectToAction("Atualizar", new { projetoId = equipe.EquipeId }).Success("Integrante da equipe atualizado com sucesso.");
            }
            else
            {
                var nomeDaImgEquipe = Path.GetFileName(fotoEquipe.FileName);
                var pathEquipe = Path.Combine(Server.MapPath("~/Uploads/equipe/"), nomeDaImgEquipe);
                fotoEquipe.SaveAs(pathEquipe);
                var imagem = _imagem.BuscarPorId(equipe.ImagemId);
                imagem.Url = imagem.CaminhoImgEquipe(fotoEquipe.FileName);
                _imagem.Atualizar(imagem);
                _equipe.Atualizar(equipe);
                return RedirectToAction("Atualizar", new { projetoId = equipe.EquipeId }).Success("Integrante da equipe atualizado com sucesso.");
            }
        }

        public ActionResult Ativo(Guid equipeId, int pagina)
        {
            var equipeAtual = _equipe.BuscarPorId(equipeId);
            equipeAtual.AtivarOuDesativar();
            _equipe.Atualizar(equipeAtual);
            TempData["Pagina"] = pagina;
            int itensPorPagina = 8;
            var equipes = _equipe.BuscarTodos();
            var lista = new List<Equipe>();
            lista.AddRange(equipes);
            return PartialView("_PartialTabela", lista.ToPagedList(pagina, itensPorPagina));
        }

        public PartialViewResult BuscarEquipe(string textoDigitado, int pagina = 1)
        {
            TempData["Pagina"] = pagina;
            int itensPorPagina = 8;
            var equipes = _equipe.BuscarTodosPorEquipe(textoDigitado);
            var lista = new List<Equipe>();
            lista.AddRange(equipes);
            return PartialView("_PartialTabela", lista.ToPagedList(pagina, itensPorPagina));
        }
    }
}