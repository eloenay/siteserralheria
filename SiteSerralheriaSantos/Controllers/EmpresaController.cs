﻿using PagedList;
using SiteSerralheriaSantos.Help;
using SiteSerralheriaSantos.Models;
using SiteSerralheriaSantos.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;

namespace SiteSerralheriaSantos.Controllers
{
    public class EmpresaController : Controller
    {
        private readonly Empresa _empresa;
        private readonly Endereco _endereco;
        private readonly RedeSocial _redesocial;
        private readonly Imagem _imagem;
        private readonly Usuario _usuario;
        private readonly Mensagem _mensagem;

        public EmpresaController()
        {
            _empresa = new Empresa();
            _endereco = new Endereco();
            _redesocial = new RedeSocial();
            _imagem = new Imagem();
            _usuario = new Usuario();
            _mensagem = new Mensagem();
        }

        public ActionResult Index(int pagina = 1)
        {
            ViewBag.Mensagem = _mensagem.QntDeMsgNaoVisualizada();
            TempData["Pagina"] = pagina;
            int itensPorPagina = 8;
            var empresas = _empresa.BuscarTodos();
            var lista = new List<Empresa>();
            lista.AddRange(empresas);
            return View(lista.ToPagedList(pagina, itensPorPagina));
        }

        public ActionResult AddOrUpdate()
        {
            var usuario = _usuario.BuscarPorId(Account.UsuarioId);
            var empresa = (usuario.EmpresaId == null) ? null : _empresa.BuscarPorId((Guid)usuario.EmpresaId);
            ViewBag.Empresa = (empresa == null) ? null : empresa;
            ViewBag.Mensagem = _mensagem.QntDeMsgNaoVisualizada();
            if (usuario.EmpresaId == null)
                return RedirectToAction("Adicionar");
            else
                return RedirectToAction("Atualizar", new { empresaId = usuario.EmpresaId });
        }

        public ActionResult Adicionar()
        {
            ViewBag.Mensagem = _mensagem.QntDeMsgNaoVisualizada();
            return View();
        }

        [HttpPost]
        public ActionResult Adicionar(Empresa empresa, HttpPostedFileBase fotoLogo)
        {
            if (!ModelState.IsValid) return View(empresa).Error("Erro ao cadastrar empresa");
            if (fotoLogo != null)
            {
                var nomeDaImgDaLogo = Path.GetFileName(fotoLogo.FileName);
                var pathEmpresa = Path.Combine(Server.MapPath("~/Uploads/logo/"), nomeDaImgDaLogo);
                fotoLogo.SaveAs(pathEmpresa);
                var imagem = new Imagem();
                imagem.Url = imagem.CaminhoImgLogoEmpresa(fotoLogo.FileName);
                imagem.ImagemId = Guid.NewGuid();
                _imagem.Adicionar(imagem);
                empresa.ImagemId = imagem.ImagemId;
                empresa.EmpresaId = Guid.NewGuid();
                _empresa.Adicionar(empresa);
                var usuario = _usuario.BuscarPorId(Account.UsuarioId);
                if (usuario.EmpresaId == null && usuario.TipoUsuario == TipoUsuario.Administrador)
                {
                    usuario.EmpresaId = (Guid?)empresa.EmpresaId;
                    _usuario.Atualizar(usuario);
                }
                return RedirectToAction("Atualizar", new { empresaId = empresa.EmpresaId }).Success("Empresa cadastrado com sucesso");
            }
            else
            {
                return View(empresa).Error("Erro ao cadastrar empresa");
            }
        }

        public ActionResult Atualizar(Guid empresaId)
        {
            ViewBag.Mensagem = _mensagem.QntDeMsgNaoVisualizada();
            return View(_empresa.BuscarPorId(empresaId));
        }

        [HttpPost]
        public ActionResult Atualizar(Empresa empresa, HttpPostedFileBase fotoLogo)
        {
            if (!ModelState.IsValid) return View(empresa).Error("Erro ao atualizar empresa");
            if (fotoLogo == null)
            {
                _empresa.Atualizar(empresa);
                return RedirectToAction("Atualizar", new { empresaId = empresa.EmpresaId }).Success("Empresa atualizado com sucesso");
            }
            else
            {
                var nomeDaImgDaLogo = Path.GetFileName(fotoLogo.FileName);
                var pathEmpresa = Path.Combine(Server.MapPath("~/Uploads/logo/"), nomeDaImgDaLogo);
                fotoLogo.SaveAs(pathEmpresa);
                var imagem = _imagem.BuscarPorId(empresa.ImagemId);
                imagem.Url = imagem.CaminhoImgLogoEmpresa(fotoLogo.FileName);
                _imagem.Atualizar(imagem);
                _empresa.Atualizar(empresa);
                return RedirectToAction("Atualizar", new { empresaId = empresa.EmpresaId }).Success("Empresa atualizado com sucesso");
            }
        }

        public ActionResult Ativo(Guid empresaId, int pagina)
        {
            var empresaAtual = _empresa.BuscarPorId(empresaId);
            empresaAtual.AtivarOuDesativar();
            _empresa.Atualizar(empresaAtual);
            TempData["Pagina"] = pagina;
            int itensPorPagina = 8;
            var empresas = _empresa.BuscarTodos();
            var lista = new List<Empresa>();
            lista.AddRange(empresas);
            return PartialView("_PartialTabela", lista.ToPagedList(pagina, itensPorPagina));
        }

        public PartialViewResult BuscarEmpresas(string textoDigitado, int pagina = 1)
        {
            TempData["Pagina"] = pagina;
            int itensPorPagina = 8;
            var empresas = _empresa.BuscarTodosPorEmpresa(textoDigitado);
            var lista = new List<Empresa>();
            lista.AddRange(empresas);
            return PartialView("_PartialTabela", lista.ToPagedList(pagina, itensPorPagina));
        }

        public ActionResult AbrirModalAdicionarEndereco()
        {
            return PartialView("_ModalAdicionarEndereco");
        }

        public ActionResult AbrirModalAdicionarRedeSocial()
        {
            return PartialView("_ModalAdicionarRedeSocial");
        }

        public ActionResult AbrirModalAtualizarEndereco(Guid enderecoId)
        {
            var endereco = _endereco.BuscarPorId(enderecoId);
            return PartialView("_ModalAtualizarEndereco", endereco);
        }

        public ActionResult AbrirModalAtualizarRedeSocial(Guid redesocialId)
        {
            var redesocial = _redesocial.BuscarPorId(redesocialId);
            return PartialView("_ModalAtualizarRedeSocial", redesocial);
        }

        [HttpPost]
        public ActionResult AdicionarEndereco(Endereco endereco)
        {
            _endereco.Adicionar(endereco);
            var enderecos = _endereco.BuscarTodosEnderecoPorEmpresa(endereco.EmpresaId);
            return PartialView("_PartialTabelaEndereco", enderecos);
        }

        [HttpPost]
        public ActionResult AdicionarRedeSocial(RedeSocial redesocial)
        {
            _redesocial.Adicionar(redesocial);
            var redesociais = _redesocial.BuscarTodosRedeSocialPorEmpresa(redesocial.EmpresaId);
            return PartialView("_PartialTabelaRedeSocial", redesociais);
        }

        [HttpPost]
        public ActionResult AtualizarEndereco(Endereco endereco)
        {
            _endereco.Atualizar(endereco);
            var enderecos = _endereco.BuscarTodosEnderecoPorEmpresa(endereco.EmpresaId);
            return PartialView("_PartialTabelaEndereco", enderecos);
        }

        [HttpPost]
        public ActionResult AtualizarRedeSocial(RedeSocial redesocial)
        {
            _redesocial.Atualizar(redesocial);
            var redesociais = _redesocial.BuscarTodosRedeSocialPorEmpresa(redesocial.EmpresaId);
            return PartialView("_PartialTabelaRedeSocial", redesociais);
        }

        public ActionResult AtivoEndereco(Guid enderecoId)
        {
            var enderecoAtual = _endereco.BuscarPorId(enderecoId);
            enderecoAtual.AtivarOuDesativar();
            _endereco.Atualizar(enderecoAtual);
            var enderecos = _endereco.BuscarTodosEnderecoPorEmpresa(enderecoAtual.EmpresaId);
            return PartialView("_PartialTabelaEndereco", enderecos);
        }

        public ActionResult AtivoRedeSocial(Guid redesocialId)
        {
            var redesocialAtual = _redesocial.BuscarPorId(redesocialId);
            redesocialAtual.AtivarOuDesativar();
            _redesocial.Atualizar(redesocialAtual);
            var redesociais = _redesocial.BuscarTodosRedeSocialPorEmpresa(redesocialAtual.EmpresaId);
            return PartialView("_PartialTabelaRedeSocial", redesociais);
        }
    }
}