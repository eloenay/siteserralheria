using SiteSerralheriaSantos.Models;
using System;
using System.Data.Entity.Migrations;

namespace SiteSerralheriaSantos.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<SiteSerralheriaSantos.Context.ConexaoBanco>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(SiteSerralheriaSantos.Context.ConexaoBanco context)
        {
            context.Usuario.AddOrUpdate(u => u.Login,
               new Usuario()
               {
                   UsuarioId = Guid.NewGuid(),
                   Nome = "ELOENAY PEREIRA",
                   Login = "eloenay",
                   Senha = "8D969EEF6ECAD3C29A3A629280E686CFC3F5D5A86AFF3CA122C923ADC6C92",//senha = 123456
                   Ativo = true,
                   TipoUsuario = TipoUsuario.Master
               });
        }
    }
}
