﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SiteSerralheriaSantos.ViewModels
{
    public class LoginVm
    {
        [HiddenInput]
        public string UrlRetorno { get; set; }

        [Required(ErrorMessage = "Informe o login")]
        [Display(Name = "Email *")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Informe a senha")]
        [DataType(DataType.Password)]
        [Display(Name = "Senha *")]
        public string Senha { get; set; }

    }
}