﻿$(function () {

    "use script";

    window.CarregarImagem = window.CarregarImagem || {};

    CarregarImagem.Apoio = {

        Acoes: () => {
            $(document).on("click", "#close-preview", function () {
                $(".image-preview").popover("hide");
                $(".image-preview").hover(
                    function () {
                        $(".image-preview").popover("show");
                    },
                    function () {
                        $(".image-preview").popover("hide");
                    }
                );
            });

            var closebtn = $("<button/>", {
                type: "button",
                text: "x",
                id: "close-preview",
                style: "font-size: initial;"
            });

            closebtn.attr("class", "close pull-right");

            $(".image-preview").popover({
                trigger: 'manual',
                html: true,
                title: "<strong>Imagem Escolhida</strong>" + $(closebtn)[0].outerHTML,
                content: "There's no image",
                placement: 'bottom'
            });

            $(".image input:file").change(function () {
                var img = $("<img/>", {
                    id: "dynamic",
                    width: 250,
                    height: 200
                });

                var file = this.files[0];
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(".image-preview-filename").val(file.name);
                    img.attr('src', e.target.result);
                    $(".image-preview").attr("data-content", $(img)[0].outerHTML).popover("show");
                };
                reader.readAsDataURL(file);
            });
        }
    };

    CarregarImagem.Apoio.Acoes();
});