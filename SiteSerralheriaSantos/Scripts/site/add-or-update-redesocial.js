﻿$(function () {

    "use string";

    window.AddOrUpdateRedeSocial = window.AddOrUpdateRedeSocial || {};

    AddOrUpdateRedeSocial.CarregarEvento = function () {
        $(document).ready(function () {
            $(document).on("click", "#btn-adicionar-redesocial", function () {
                AddOrUpdateRedeSocial.Adicionar();
            });

            $(document).on("click", "#btn-atualizar-redesocial", function () {
                AddOrUpdateRedeSocial.Atualizar();
            });
        });
    };

    AddOrUpdateRedeSocial.CriarEntidadeRedeSocial = function () {
        this.RedeSocialId = $("#redesocialId").val();
        this.TipoRedeSocial = $("#titulo").val();
        this.Link = $("#link").val();
        this.EmpresaId = $("#EmpresaId").val();
    };

    AddOrUpdateRedeSocial.Adicionar = function () {
        var tblRedeSocial = $(".tbl-redesocial");
        var url = $("#formAddRedeSocial").attr("action");
        var dados = new AddOrUpdateRedeSocial.CriarEntidadeRedeSocial();
        $.ajax({
            url: url,
            data: { redesocial: dados },
            method: "POST",
            success: function (result) {
                tblRedeSocial.empty();
                tblRedeSocial.append(result);
                $(".abrir-modal").modal("hide");
            },
            error: function (e) {
                alert(e);
            }
        });
    };

    AddOrUpdateRedeSocial.Atualizar = function () {
        var tblRedeSocial = $(".tbl-redesocial");
        var url = $("#formUpdateRedeSocial").attr("action");
        var dados = new AddOrUpdateRedeSocial.CriarEntidadeRedeSocial();
        $.ajax({
            url: url,
            data: { redesocial: dados },
            method: "POST",
            success: function (result) {
                tblRedeSocial.empty();
                tblRedeSocial.html(result);
                $(".abrir-modal").modal("hide");
            },
            error: function (e) {
                alert(e);
            }
        });
    };

    AddOrUpdateRedeSocial.CarregarEvento();
});