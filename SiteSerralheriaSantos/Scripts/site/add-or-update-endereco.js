﻿$(function () {

    "use string";

    window.AddOrUpdateEndereco = window.AddOrUpdateEndereco || {};

    AddOrUpdateEndereco.CarregarEvento = function () {
        $(document).ready(function () {
            $(document).on("click", "#btn-adicionar-endereco", function () {
                AddOrUpdateEndereco.Adicionar();
            });

            $(document).on("click", "#btn-atualizar-endereco", function () {
                AddOrUpdateEndereco.Atualizar();
            });
        });
    };

    AddOrUpdateEndereco.CriarEntidadeEndereco = function () {
        this.EnderecoId = $("#enderecoId").val();
        this.Rua = $("#rua").val();
        this.Numero = $("#numero").val();
        this.Bairro = $("#bairro").val();
        this.Cep = $("#cep").val();
        this.Cidade = $("#cidade").val();
        this.Estado = $("#estado").val();
        this.Frame = $("#frame").val();
        this.EmpresaId = $("#EmpresaId").val();
    };

    AddOrUpdateEndereco.Adicionar = function () {
        var tblEndereco = $(".tbl-endereco");
        var url = $("#formAddEndereco").attr("action");
        var dados = new AddOrUpdateEndereco.CriarEntidadeEndereco();
        $.ajax({
            url: url,
            data: { endereco: dados },
            method: "POST",
            success: function (result) {
                tblEndereco.empty();
                tblEndereco.append(result);
                $(".abrir-modal").modal("hide");
            },
            error: function (e) {
                alert(e);
            }
        });
    };

    AddOrUpdateEndereco.Atualizar = function () {
        var tblEndereco = $(".tbl-endereco");
        var url = $("#formUpdateEndereco").attr("action");
        var dados = new AddOrUpdateEndereco.CriarEntidadeEndereco();
        $.ajax({
            url: url,
            data: { endereco: dados },
            method: "POST",
            success: function (result) {
                tblEndereco.empty();
                tblEndereco.html(result);
                $(".abrir-modal").modal("hide");
            },
            error: function (e) {
                alert(e);
            }
        });
    };

    AddOrUpdateEndereco.CarregarEvento();
});