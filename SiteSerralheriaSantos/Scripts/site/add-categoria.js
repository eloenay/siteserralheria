﻿$(function () {

    "use string";

    window.AddCategoria = window.AddCategoria || {};

    AddCategoria.CarregarEvento = function () {
        $(document).ready(function () {
            $(document).on("click", "#btn-adicionar-categoria", function () {
                AddCategoria.Adicionar();
            });
        });
    };

    AddCategoria.CriarEntidadeCategoria = function () {
        this.Nome = $("#nome").val();
    };

    AddCategoria.Adicionar = function () {
        var url = $("#formAddCategoria").attr("action");
        var dados = new AddCategoria.CriarEntidadeCategoria();
        $.ajax({
            url: url,
            data: { categoria: dados },
            method: "POST",
            success: function () {
                $(".abrir-modal").modal("hide");
                window.location.reload();
            },
            error: function (e) {
                alert(e);
            }
        });
    };

    AddCategoria.CarregarEvento();
});