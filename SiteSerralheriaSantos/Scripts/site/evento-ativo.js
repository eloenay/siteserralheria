﻿$(function () {

    "use string";

    window.EventoAtivo = window.EventoAtivo || {};

    EventoAtivo.Ativo = function () {
        $(document).ready(function () {
            $(document).on("click", "#btn-ativo", function () {
                var paginaAtual = $("#pageLink").data("paginaatual");
                var divQueCarregaItens = $(".carregar-itens");
                divQueCarregaItens.empty();
                divQueCarregaItens.html("<div id='div-loading'><img id='img-loading' src='../../imagem/loading.gif' /></div>");
                var url = $(this).data("ativo");

                $.ajax({
                    url: url,
                    data: { pagina: paginaAtual },
                    method: "POST",
                    success: function (result) {
                        divQueCarregaItens.empty();
                        divQueCarregaItens.append(result);
                        swal("Mensagem", "Informação ativada/desativada com sucesso", "success");
                    },
                    error: function () {
                        swal("Mensagem", "Erro ao ativar/desativar informação", "error");
                    }
                });
            });
        });
    };

    EventoAtivo.AtivoRedeSocial = function () {
        $(document).ready(function () {
            $(document).on("click", "#btn-ativoRedeSocial", function () {
                var tblSubMenu = $(".tbl-redesocial");
                tblSubMenu.empty();
                tblSubMenu.html("<div id='div-loading'><img id='img-loading' src='../../imagem/loading.gif' /></div>");
                var url = $(this).data("ativo");
                $.ajax({
                    url: url,
                    method: "POST",
                    success: function (result) {
                        tblSubMenu.empty();
                        tblSubMenu.append(result);
                        swal("Mensagem", "Informação ativada/desativada com sucesso", "success");
                    },
                    error: function () {
                        swal("Mensagem", "Erro ao ativar/desativar informação", "error");
                    }
                });
            });
        });
    };

    EventoAtivo.Ativo();
    EventoAtivo.AtivoRedeSocial();
});