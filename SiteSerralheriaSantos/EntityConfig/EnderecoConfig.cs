﻿using SiteSerralheriaSantos.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SiteSerralheriaSantos.EntityConfig
{
    public class EnderecoConfig : EntityTypeConfiguration<Endereco>
    {
        public EnderecoConfig()
        {
            HasKey(x => x.EnderecoId);
            Property(x => x.EnderecoId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Rua).IsRequired();
            Property(x => x.Numero).IsRequired();
            Property(x => x.Bairro).IsRequired();
            Property(x => x.Cidade).IsRequired();
            Property(x => x.Estado).IsRequired();
            Property(x => x.Cep).IsRequired();
            Property(x => x.Ativo).IsRequired();
            Property(x => x.Frame).IsRequired();

            HasRequired(l => l.Empresa).WithMany(x => x.Enderecos).HasForeignKey(x => x.EmpresaId).WillCascadeOnDelete(false);

            ToTable(nameof(Endereco));
        }
    }
}