﻿using SiteSerralheriaSantos.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SiteSerralheriaSantos.EntityConfig
{
    public class EmpresaConfig : EntityTypeConfiguration<Empresa>
    {
        public EmpresaConfig()
        {
            HasKey(x => x.EmpresaId);
            Property(x => x.EmpresaId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.Nome).IsRequired().HasMaxLength(250);
            Property(x => x.Celular).IsRequired();
            Property(x => x.CelularWhatsapp).IsRequired();
            Property(x => x.Telefone).IsRequired();
            Property(x => x.Email).IsRequired();
            Property(x => x.QuemSomos).IsRequired();
            Property(x => x.Informativo).IsRequired();
            Property(x => x.Missao).IsRequired();
            Property(x => x.Visao).IsRequired();
            Property(x => x.Valores).IsRequired();
            Property(x => x.Ativo).IsRequired();

            HasRequired(x => x.Imagem).WithMany(x => x.Empresas).HasForeignKey(x => x.ImagemId).WillCascadeOnDelete(false);

            ToTable(nameof(Empresa));
        }
    }
}