﻿using SiteSerralheriaSantos.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SiteSerralheriaSantos.EntityConfig
{
    public class CategoriaConfig : EntityTypeConfiguration<Categoria>
    {
        public CategoriaConfig()
        {
            HasKey(x => x.CategoriaId);
            Property(x => x.CategoriaId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Nome).IsRequired();
            Property(x => x.Ativo).IsRequired();

            ToTable(nameof(Categoria));
        }
    }
}