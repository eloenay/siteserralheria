﻿using SiteSerralheriaSantos.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SiteSerralheriaSantos.EntityConfig
{
    public class ImagemConfig : EntityTypeConfiguration<Imagem>
    {
        public ImagemConfig()
        {
            HasKey(x => x.ImagemId);
            Property(x => x.ImagemId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.Url).IsRequired();

            ToTable(nameof(Imagem));
        }
    }
}