﻿using SiteSerralheriaSantos.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SiteSerralheriaSantos.EntityConfig
{
    public class ParceiroConfig : EntityTypeConfiguration<Parceiro>
    {
        public ParceiroConfig()
        {
            HasKey(x => x.ParceiroId);
            Property(x => x.ParceiroId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Nome).IsRequired();
            Property(x => x.Link).IsRequired();
            Property(x => x.Ativo).IsRequired();

            HasRequired(l => l.Empresa).WithMany(x => x.Parceiros).HasForeignKey(x => x.EmpresaId).WillCascadeOnDelete(false);
            HasRequired(x => x.Imagem).WithMany(x => x.Parceiros).HasForeignKey(x => x.ImagemId).WillCascadeOnDelete(false);

            ToTable(nameof(Parceiro));
        }
    }
}