﻿using SiteSerralheriaSantos.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SiteSerralheriaSantos.EntityConfig
{
    public class EquipeConfig : EntityTypeConfiguration<Equipe>
    {
        public EquipeConfig()
        {
            HasKey(x => x.EquipeId);
            Property(x => x.EquipeId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Nome).IsRequired();
            Property(x => x.Cargo).IsRequired();
            Property(x => x.Ativo).IsRequired();

            HasRequired(l => l.Empresa).WithMany(x => x.Equipes).HasForeignKey(x => x.EmpresaId).WillCascadeOnDelete(false);
            HasRequired(l => l.Imagem).WithMany(x => x.Equipes).HasForeignKey(x => x.ImagemId).WillCascadeOnDelete(false);

            ToTable(nameof(Equipe));
        }
    }
}