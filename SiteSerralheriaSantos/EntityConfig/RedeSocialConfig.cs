﻿using SiteSerralheriaSantos.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SiteSerralheriaSantos.EntityConfig
{
    public class RedeSocialConfig : EntityTypeConfiguration<RedeSocial>
    {
        public RedeSocialConfig()
        {
            HasKey(x => x.RedeSocialId);
            Property(x => x.RedeSocialId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.TipoRedeSocial).IsRequired();
            Property(x => x.Link).IsRequired();
            Property(x => x.Ativo).IsRequired();

            HasRequired(l => l.Empresa).WithMany(x => x.RedesSociais).HasForeignKey(x => x.EmpresaId).WillCascadeOnDelete(false);

            ToTable(nameof(RedeSocial));
        }
    }
}