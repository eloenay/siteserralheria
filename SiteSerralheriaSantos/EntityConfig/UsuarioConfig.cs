﻿using SiteSerralheriaSantos.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SiteSerralheriaSantos.EntityConfig
{
    public class UsuarioConfig : EntityTypeConfiguration<Usuario>
    {
        public UsuarioConfig()
        {
            HasKey(x => x.UsuarioId);
            Property(x => x.UsuarioId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Nome).IsRequired().HasMaxLength(250);
            Property(x => x.Login).IsRequired().HasMaxLength(250);
            Property(x => x.Senha).IsRequired().HasMaxLength(250);
            Property(x => x.Ativo).IsRequired();
            Property(x => x.TipoUsuario).IsRequired();

            HasOptional(l => l.Empresa).WithMany(x => x.Usuarios).HasForeignKey(x => x.EmpresaId).WillCascadeOnDelete(false);

            ToTable(nameof(Usuario));
        }
    }
}