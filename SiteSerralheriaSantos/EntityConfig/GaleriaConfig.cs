﻿using SiteSerralheriaSantos.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SiteSerralheriaSantos.EntityConfig
{
    public class GaleriaConfig : EntityTypeConfiguration<Galeria>
    {
        public GaleriaConfig()
        {
            HasKey(x => x.GaleriaId);
            Property(x => x.GaleriaId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Titulo).IsRequired();
            Property(x => x.Ativo).IsRequired();

            HasRequired(l => l.Empresa).WithMany(x => x.Galerias).HasForeignKey(x => x.EmpresaId).WillCascadeOnDelete(false);
            HasRequired(x => x.Imagem).WithMany(x => x.Galerias).HasForeignKey(x => x.ImagemId).WillCascadeOnDelete(false);
            HasRequired(x => x.Categoria).WithMany(x => x.Galerias).HasForeignKey(x => x.CategoriaId).WillCascadeOnDelete(false);

            ToTable(nameof(Galeria));
        }
    }
}