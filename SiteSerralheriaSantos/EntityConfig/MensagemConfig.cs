﻿using SiteSerralheriaSantos.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SiteSerralheriaSantos.EntityConfig
{
    public class MensagemConfig : EntityTypeConfiguration<Mensagem>
    {
        public MensagemConfig()
        {
            HasKey(x => x.MensagemId);
            Property(x => x.MensagemId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Nome).IsRequired();
            Property(x => x.Email).IsRequired();
            Property(x => x.Texto).IsRequired();
            Property(x => x.DataDeEntrada).IsRequired();
            Property(x => x.Visto).IsRequired();

            HasRequired(l => l.Empresa).WithMany(x => x.Mensagens).HasForeignKey(x => x.EmpresaId).WillCascadeOnDelete(false);

            ToTable(nameof(Mensagem));
        }
    }
}